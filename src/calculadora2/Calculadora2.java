/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora2;

/**
 *
 * @author nacho
 */
public class Calculadora2 {

    private int op1;
    private int op2;

    public Calculadora2(int op1, int op2) {
        this.op1 = op1;
        this.op2 = op2;
    }
    
     public int suma() {
        return op1+op2;
    }
    
    public int resta() {
        return op1-op2;
    }
    
    public int producto() {
        return op1*op2;
    }
    
    public int cociente() {
        return op1/op2;
    }    

    
}
