/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora2;

import java.util.Arrays;
import java.util.Collection;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 *
 * @author nacho
 */
@RunWith(Parameterized.class)
public class Calculadora2TestProducto {
    private int num1;
    private int num2;
    private int resultado;
    
    public Calculadora2TestProducto(int num1, int num2, int resultado) {
        this.num1=num1;
        this.num2=num2;
        this.resultado=resultado;
    }
    
    @Parameters
    public static Collection<Object[]> numeros() {
        return Arrays.asList(new Object[][]{
            {20,10,200},
            {30, -2, -60},
            {5,2,10}});
    }
    
    
    @Test
    public void testProducto() {
        System.out.println("Producto");
        assertEquals(resultado, new Calculadora2(num1,num2).producto());
    }
    
}
