/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora2;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author nachorod
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({calculadora2.Calculadora2TestSuma.class, calculadora2.Calculadora2TestCociente.class, calculadora2.Calculadora2TestProducto.class, calculadora2.Calculadora2TestResta.class})
public class Calculadora2TestSuite {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
}
